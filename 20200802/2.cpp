#include<cstdio>//uncle-lu
#include<algorithm>

int n;
unsigned long long int k, temp;

int main()
{
	scanf("%d %llu", &n, &k);
	temp = 1;
	for (int i = 1; i <= n-1; i++) 
		temp <<= 1;

	int flag = 0;
	for (int i = 1; i <= n; i++) 
	{
		if(k <= temp)
			printf("%d", flag);
		else
		{
			k -= temp;
			flag = !flag;
			printf("%d", flag);
		}
		temp >>= 1;
	}

	return 0;
}
