#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int line[100010], ti[100010];
bool visit[100010];
int head, last;
int n, ans;

int main()
{
	scanf("%d", &n);
	head = 1;
	int model, price, t;
	for (int i = 1; i <= n; i++) 
	{
		scanf("%d %d %d", &model, &price, &t);
		if(model == 0)
		{
			last++;
			line[last] = price;
			ti[last] = t;
			ans += price;
		}
		else
		{
			bool flag = false;
			for (int j = head; j <= last; j++) 
			{
				if(line[j] >= price && ti[j] + 45 >= t && visit[j] == false)
				{
					flag = true;
					visit[j] = true;
					break;
				}
			}
			if(!flag)
				ans += price;
			while(head <= last && (ti[head] + 45 < t || visit[head] ))
				head++;
		}
	}

	printf("%d", ans);
	return 0;
}
