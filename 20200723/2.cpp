#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

const int mod = 100000000-3;
struct node{
	int x, sit;
};
int n;
node line1[1000010], line2[1000010];
int line[1000010], temp[1000010], ans;

bool cmp(node a, node b)
{
	return a.x < b.x;
}

void merge_sort(int left, int right)
{
	if(left == right)return ;
	int mid = (left + right) >> 1;

	merge_sort(left, mid);
	merge_sort(mid+1, right);

	for (int i = left; i <= right; i++) 
		temp[i] = line[i];

	int i1 = left, i2 = mid+1;
	for (int curr = left; curr <= right; curr++) 
	{
		if( i1 == mid+1 )
			line[curr] = temp[i2++];
		else if( i2 == right+1 )
			line[curr] = temp[i1++];
		else if( temp[i1] > temp[i2] )
		{
			ans += (mid-i1 + 1);
			ans %= mod;
			line[curr] = temp[i2++];
		}
		else
			line[curr] = temp[i1++];
	}

	return ;
}

int main()
{
	read(n);
	for (int i = 1; i <= n; i++) 
	{
		read(line1[i].x);
		line1[i].sit = i;
	}
	std::sort(line1+1, line1+1+n, cmp);
	for (int i = 1; i <= n; i++) 
	{
		read(line2[i].x);
		line2[i].sit = i;
	}
	std::sort(line2+1, line2+1+n, cmp);

	for (int i = 1; i <= n; i++) 
		line[line1[i].sit] = line2[i].sit;

	merge_sort(1, n);

	printf("%d", ans%mod);

	return 0;
}
