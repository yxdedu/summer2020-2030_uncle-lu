#include<cstdio>//uncle-lu
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int work(int n, int i, int j)
{
	if(i == 1)//第1行
		return j;
	if(j == n)
		return n + i - 1;
	if(i == n)
		return 3 * n - 2 - j + 1;
	if(j == 1)
		return 4 * n - 4 - i + 2;

	return work(n - 2, i - 1, j - 1) + 4 * n - 4;
}

int main()
{
	int n, a, b;
	read(n);read(a);read(b);
	printf("%d", work(n, a, b));
	return 0;
}
