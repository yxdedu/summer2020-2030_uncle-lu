#include<cstdio>//uncle-lu
#include<algorithm>

double a, b, l;

int main()
{
	scanf("%lf %lf %lf", &a, &b, &l);

	double min = 0x7f7f7f7f, ans_a, ans_b;
	for (double i = 1; i <= l; i++) 
	{
		for (double j = 1; j <= l; j++) 
		{
			if( i/j >= a/b && i/j - a/b < min && std::__gcd((int)i, (int)j) == 1)
			{
				ans_a = i;
				ans_b = j;
				min = i/j - a/b;
			}
		}
	}

	printf("%.0lf %.0lf", ans_a, ans_b);
	return 0;
}
