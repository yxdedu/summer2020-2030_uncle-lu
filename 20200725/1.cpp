#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n;
int line[200010];
int visit[200010];
bool this_visit[200010];
int ans = 0x7f7f7f7f;

void DFS(int x)
{
	if(visit[line[x]])
	{
		if(!this_visit[line[x]])return ;
		ans = std::min(ans, visit[x] - visit[line[x]] + 1);
		return ;
	}

	visit[line[x]] = visit[x] + 1;
	this_visit[line[x]] = true;
	DFS(line[x]);

	return ;
}

int main()
{
	read(n);
	for (int i = 1; i <= n; i++) 
		read(line[i]);
	for (int i = 1; i <= n; i++) 
	{
		if(!visit[i])
		{
			memset(this_visit, false, sizeof(this_visit));
			this_visit[i] = true;
			DFS(i);
		}
	}
	printf("%d", ans);
	return 0;
}
