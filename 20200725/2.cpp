#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n, m, ans;
int number[3010], color[3010];

int main()
{
	read(n);read(m);
	for (int i = 1; i <= n; i++) 
		read(number[i]);
	for (int i = 1; i <= n; i++) 
		read(color[i]);

	for (int x = 1; x <= n; x++) 
	{
		for (int z = x+1; z <= n; z++) 
		{
			if( (x + z)%2 == 1 )continue;
			if( color[x] != color[z] )continue;
			ans += (( x + z ) * (number[x] + number[z]));
			ans %= 10007;
		}
	}

	printf("%d", ans);
	return 0;
}
