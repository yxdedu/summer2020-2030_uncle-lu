#include<cstdio>//uncle-lu
#include<cmath>
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n;
int F[32768][5];

int main()
{
	F[0][0] = 1;
	for (int k = 1; k*k <= 32768; k++) //枚举平方数
		for (int x = k*k; x <= 32789; x++) //	枚举原数
			for (int j = 1; j <= 4; j++)  //枚举次数
				F[x][j] += F[x - k*k][j-1];

	int T;
	read(T);

	for (int t = 1; t <= T; t++) 
	{
		read(n);
		printf("%d\n", F[n][1] + F[n][2] + F[n][3] + F[n][4]);
	}

	return 0;
}
