#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int dis[35][35];

int main()
{
	int n, ans;
	while(true)
	{
		read(n);
		if(!n)break;
		memset(dis, 0, sizeof(dis));
		for (int i = 1; i <= n-1; i++) 
			for (int j = i+1; j <= n; j++) 
				read(dis[i][j]);

		ans = dis[1][2];

		for (int i = 3; i <= n; i++) 
		{
			int temp = (dis[1][i] - dis[1][2] + dis[2][i] )/ 2;
			for (int j = 2; j <= i-1; j++) 
				temp = std::min(temp, (dis[1][i] - dis[1][j] + dis[j][i])/2);
			ans += temp;
		}

		printf("%d\n",ans);
	}
	return 0;
}
