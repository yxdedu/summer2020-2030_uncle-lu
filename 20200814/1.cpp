#include<cstdio>//uncle-lu
#include<algorithm>

struct node{
	int x, y;
};
node line[25610];
int n, cnt;

int gcd(int x, int y)
{
	if(x%y == 0)
		return y;
	return gcd(y, x%y);
}
bool cmp(node a, node b)
{
	double c1 = (double)a.y / (double)a.x;
	double c2 = (double)b.y / (double)b.x;
	return c1 < c2;
}
int main()
{
	scanf("%d", &n);
	cnt++;
	line[cnt].x = 1;
	line[cnt].y = 0;
	cnt++;
	line[cnt].x = 1;
	line[cnt].y = 1;
	for (int b = 1; b <= n; b++) 
		for (int a = 1; a <= b-1; a++) 
			if( gcd(a, b) == 1 )
			{
				line[++cnt].x = b;
				line[cnt].y = a;
			}

	std::sort(line+1, line+1+cnt, cmp);

	for (int i = 1; i <= cnt; i++) 
		printf("%d/%d\n", line[i].y, line[i].x);

	return 0;
}
