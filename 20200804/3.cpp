#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

long long int a, b;

int main()
{
	int T;
	read(T);
	for (int t = 1; t <= T; t++) 
	{
		read(a);read(b);
		int a_mod = a % 9;
		int b_mod = b % 9;
		
		if(b - a + 1 <= 9)
		{
			long long int sum = 0;
			for (long long int i = a; i <= b; i++) 
				sum += i;
			printf("%lld\n", sum%9);
		}
		else
		{
			int sum = 0;
			for (int i = a_mod; i <= 9; i++) 
				sum += i;
			for (int i = b_mod; i >= 1; i--) 
				sum += i;
			printf("%d\n", sum%9);
		}
	}

	return 0;
}
