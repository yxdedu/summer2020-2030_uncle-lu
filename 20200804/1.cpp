#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n, m, ans;
int line[30];
bool sit[30];
bool F[2010];

void check()
{
	memset(F, false, sizeof(F));
	int tot = 0, cnt = 0;
	F[0] = true;
	for (int i = 1; i <= n; i++) 
	{
		if(sit[i])continue;
		for (int j = tot; j >= 0; j--) 
		{
			if(F[j] && !F[j+line[i]])
			{
				F[j+line[i]] = true;
				cnt ++;
			}
		}
		tot += line[i];
	}

	ans = std::max(ans, cnt);
	return ;
}

void DFS(int x, int cnt)
{
	if(cnt > m)
		return ;
	if(x == n + 1 )
	{
		if(cnt == m)
			check();
		return ;
	}

	DFS(x+1, cnt);
	sit[x] = true;
	DFS(x+1, cnt+1);
	sit[x] = false;

	return ;
}

int main()
{
	read(n);read(m);
	for (int i = 1; i <= n; i++) 
		read(line[i]);

	DFS(1, 0);

	printf("%d", ans);
	return 0;
}
