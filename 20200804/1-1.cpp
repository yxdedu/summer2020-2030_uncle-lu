#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n, m, ans;
int line[30];
bool sit[30];
bool F[30][2010];

void check()
{
	memset(F, false, sizeof(F));
	int tot = 0, cnt = 0;
	F[0][0] = true;
	for (int i = 1; i <= n; i++) 
	{
		if(sit[i])
		{
			for (int j = 0; j <= tot; j++) 
				F[i][j] = F[i-1][j];
			continue;
		}
		tot += line[i];
		for (int j = line[i]; j <= tot; j++) 
			F[i][j] |= F[i-1][j-line[i]];
	}
	for (int i = 1; i <= tot; i++) 
		if(F[n][i])
			cnt++;
	ans = std::max(ans, cnt);
	return ;
}

void DFS(int x, int cnt)
{
	if(cnt > m)
		return ;
	if(x == n + 1 )
	{
		if(cnt == m)
			check();
		return ;
	}

	DFS(x+1, cnt);
	sit[x] = true;
	DFS(x+1, cnt+1);
	sit[x] = false;

	return ;
}

int main()
{
	read(n);read(m);
	for (int i = 1; i <= n; i++) 
		read(line[i]);

	DFS(1, 0);

	printf("%d", ans);
	return 0;
}
