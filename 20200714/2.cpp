#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int head=1,last=1,n,m,cnt;
int l[10010];

bool search(int x)
{
	for(int i=last;i<head;++i)
		if(l[i]==x)return true;
	return false;
}

void add(int x)
{
	if(head-last>m-1)
	{
		last++;
	}
	l[head]=x;
	head++;
}

int main()
{
	int x;
	read(m);read(n);
	for(int i=1;i<=n;++i)
	{
		read(x);
		if(search(x))
			continue;
		cnt++;
		add(x);
	}
	printf("%d",cnt);
	return 0;
}