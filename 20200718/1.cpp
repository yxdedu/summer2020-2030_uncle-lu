#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int mod = 20123;
int n, m;
int map[10010][110];
int visit[10010][110];
int c[10010];

int main()
{
	read(n);read(m);
	for (int i = 1; i <= n; i++) 
		for (int j = 0; j < m; j++) 
		{
			read(visit[i][j]); read(map[i][j]);
			c[i] += visit[i][j];
		}

	int ans = 0, sit = 0;
	read(sit);
	for (int i = 1; i <= n; i++) 
	{
		ans = (ans + map[i][sit]) % mod;
		map[i][sit] = map[i][sit] % c[i] + c[i];

		int temp = map[i][sit];

		while(temp)
		{
			temp -= visit[i][sit];
			if(!temp)break;
			sit = (sit + 1) % m;
		}
	}

	printf("%d", ans);

	return 0;
}
