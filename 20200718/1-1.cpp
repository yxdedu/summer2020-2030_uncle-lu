#include<cstdio>//uncle-lu
#include<algorithm>

int mod = 20123;
int n, m;
int map[10010][110];//每间房间的上面的标号.
int visit[10010][110];//每间房间是否有楼梯.

int main()
{
    scanf("%d %d", &n, &m);
    for(int i=1; i<=n; ++i)
        for(int j=0; j<m; ++j)
            scanf("%d %d", &visit[i][j], &map[i][j]);

    int ans = 0, sit = 0;// sit --->当前的房间.
    scanf("%d", &sit);
    for(int i=1; i<=n; ++i)//循环每一层的情况.
    {
        ans = (ans + map[i][sit]) % mod;

        int temp = map[i][sit];

        while(temp)
        {
            temp -= visit[i][sit];// visit[i][sit] 1 == 有楼梯的 0 == 没楼梯的;
            if(temp == 0)break;
            sit = (sit + 1) % m;
        }
    }

    printf("%d", ans);
    return 0;
}
