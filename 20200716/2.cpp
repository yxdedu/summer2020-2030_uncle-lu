#include<cstdio>//uncle-lu
#include<string>
#include<iostream>
#include<cstring>
#include<algorithm>

std::string m;
std::string data;
std::string temp;

int main()
{
	//freopen("in","r",stdin);
	getline(std::cin, m);
	int i = 0;
	while(m[i] != '\0')
	{
		if(m[i] <= 'Z' && m[i] >= 'A')
			m[i] = m[i] - 'A' + 'a';
		i++;
	}

	getline(std::cin, data);

	i = 0;
	while(data[i] != '\0')
	{
		if(data[i] <= 'Z' && data[i] >= 'A')
			data[i] = data[i] - 'A' + 'a';
		i++;
	}

	bool flag = false;
	int tot = 0, sit;
	for(i = 0; data[i]!='\0'; ++i)
	{
		while(data[i]==' '&&data[i]!='\0')i++;
		int j;
		for(j = 0; data[i + j]!=' ' && data[i+j] != '\0'; j++)
			temp+=data[i+j];

		if(temp == m)
		{
			if(!flag)
			{
				flag = true;
				sit = i;
			}
			tot++;
		}
	}

	if(flag)
		printf("%d %d", tot, sit);
	else
		printf("-1");

	return 0;
}
