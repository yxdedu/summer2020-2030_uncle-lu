#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

long long int n, p, S[1000010], line[1000010], F[1000010], sc[1000010];

int main()
{
	read(n);read(p);
	for (int i = 1; i <= n; i++) 
	{
		read(line[i]);
		S[i] = S[i-1] + line[i];
	}

	long long int mx = line[1];
	long long int mi = std::min(S[1], 0LL); 
	F[1] = mx;//特征值
	for (int i = 2; i <= n; i++) 
	{
		mx = std::max(S[i] - mi, mx);
		mi = std::min(mi, S[i]);
		F[i] = mx;
	}

	sc[1] = F[1];
	mx = (sc[1] + F[1]) % p;
	for (int i = 2; i <= n; i++) 
	{
		sc[i] = mx;
		mx = std::max(mx, sc[i] + F[i]);
	}

	mx = sc[1];
	for (int i = 2; i <= n; i++) 
		mx = std::max(mx, sc[i]);

	printf("%lld", mx%p);
}
