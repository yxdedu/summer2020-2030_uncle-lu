#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct node{
	int u, v, w;
};
node line[20010];
int n, m, s, t, mx;
int Father[10010];

int Find_Father(int x)
{
	return Father[x] == x ? x : Father[x] = Find_Father(Father[x]);
}

bool check(int x)
{
	for (int i = 1; i <= n; i++) 
		Father[i] = i;

	for (int i = 1; i <= m; i++) 
	{
		if(x < line[i].w)continue;

		int xx = Find_Father(line[i].u), yy = Find_Father(line[i].v);
		Father[yy] = xx;
	}

	if(Find_Father(s) == Find_Father(t))
		return true;
	else
		return false;
}

int main()
{
	read(n);read(m);read(s);read(t);
	for (int i = 1; i <= m; i++) 
	{
		read(line[i].u);read(line[i].v);read(line[i].w);
		mx = std::max(mx, line[i].w);
	}

	int l=0, r=mx, mid, ans;
	while(l <= r)
	{
		mid = (l + r)>>1;
		if(check(mid))
			ans = mid,
			r = mid-1;
		else
			l = mid+1;
	}

	printf("%d", ans);
	return 0;
}
