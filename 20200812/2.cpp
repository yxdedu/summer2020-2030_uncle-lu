#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct node{
	int v, next;
};
node edge[200010];
int head[100010], cnt, tot;
int F[100010];
int n, k;

void add_edge(int x, int y)
{
	edge[++cnt].v = y;
	edge[cnt].next = head[x];
	head[x] = cnt;
	return ;
}

void init()
{
	memset(head, -1, sizeof(head));
	memset(F, 0, sizeof(F));
	cnt = 0;//边数
	tot = 0;
	return ;
}

void DFS(int x, int fa)
{
	F[x] = 1;
	for (int i = head[x]; ~i; i=edge[i].next) 
	{
		int v = edge[i].v;
		if(v == fa)continue;
		DFS(v, x);
		F[x] += F[v];
	}
	if(F[x] == k)
	{
		F[x] = 0;
		tot++;
	}
	return ;
}

int main()
{
	int T;
	read(T);
	for (int t = 1; t <= T; t++) 
	{
		read(n);read(k);
		init();

		int x,y;
		for (int i = 1; i <= n-1; i++) 
		{
			read(x);read(y);
			add_edge(x, y);
			add_edge(y, x);
		}

		DFS(1, 0);

		if(tot == n/k)
			printf("YES\n");
		else
			printf("NO\n");
	}

	return 0;
}
