#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct node{
	int sc, w, sit;
};
node line[200010], temp[200010];
int n, r, q;

bool cmp(node a, node b)
{
	return a.sc > b.sc || (a.sc == b.sc && a.sit < b.sit);
}

int main()
{
	read(n); read(r); read(q);
	for (int i = 1; i <= n*2; i++) 
		line[i].sit = i;
	for (int i = 1; i <= n*2; i++) 
		read(line[i].sc);
	for (int i = 1; i <= n*2; i++) 
		read(line[i].w);
	std::sort(line+1, line+1+2*n, cmp);
	for (int i = 1; i <= r; i++) 
	{
		int i1 = 1, i2 = n+1;
		for (int j = 1; j <= n; j++) 
		{
			if(line[2*j].w > line[2*j-1].w)
			{
				temp[i1] = line[2*j];
				temp[i1].sc++;
				temp[i2] = line[2*j-1];
			}
			else
			{
				temp[i1] = line[2*j-1];
				temp[i1].sc++;
				temp[i2] = line[2*j];
			}
			i1++; i2++;
		}
		i1 = 1; i2 = n+1;
		for (int curr = 1; curr <= 2*n; curr++) 
		{
			if(i1 == n+1)
				line[curr] = temp[i2++];
			else if(i2 == 2*n+1)
				line[curr] = temp[i1++];
			else if(cmp(temp[i1], temp[i2]))
				line[curr] = temp[i1++];
			else
				line[curr] = temp[i2++];
		}
	}
	printf("%d", line[q].sit);

	return 0;
}
