#include<cstdio>//uncle-lu
#include<algorithm>
struct node{
	int sit, sc, w;
};
node line[200010];
int n, r, q;
bool cmp(node a, node b)
{
	return a.sc > b.sc || (a.sc == b.sc && a.sit < b.sit);
}
int main()
{
	scanf("%d %d %d", &n, &r, &q);
	for (int i = 1; i <= n*2; i++) 
	{
		scanf("%d", &line[i].sc);
		line[i].sit = i;
	}
	for (int i = 1; i <= n*2; i++) 
		scanf("%d", &line[i].w);

	std::sort(line+1, line+1+2*n, cmp);

	for (int i = 1; i <= r; i++) 
	{
		for (int j = 1; j <= n; j++) 
			if(line[2*j].w > line[2*j-1].w)
				line[2*j].sc ++;
			else
				line[2*j-1].sc ++;

		std::sort(line+1, line+1+2*n, cmp);
	}

	printf("%d", line[q].sit);

	return 0;
}
