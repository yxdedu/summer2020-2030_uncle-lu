#include<cstdio>//uncle-lu
#include<cstring>
#include<queue>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct node{
	int v, next;
};
node edge1[200010], edge2[200010];
int head1[10010], head2[10010], cnt1, cnt2;
int d[10010];
bool visit[10010], flag[10010];
int n, m, s, t;

void add_edge_1(int x, int y)
{
	edge1[++cnt1].v = y;
	edge1[cnt1].next=head1[x];
	head1[x] = cnt1;
	return ;
}

void add_edge_2(int x, int y)
{
	edge2[++cnt2].v = y;
	edge2[cnt2].next= head2[x];
	head2[x] = cnt2;
	return ;
}

int main()
{
	memset(head1, -1, sizeof(head1));
	memset(head2, -1, sizeof(head2));
	read(n);read(m);
	int x, y;
	for (int i = 1; i <= m; i++) 
	{
		read(x);read(y);
		add_edge_1(x, y);
		add_edge_2(y, x);
	}
	read(s);read(t);

	std::queue<int> qu;
	qu.push(t);
	visit[t] = true;
	while(!qu.empty())
	{
		int now = qu.front();
		qu.pop();
		for (int i = head2[now]; ~i; i = edge2[i].next) 
		{
			int v = edge2[i].v;
			if(!visit[v])
			{
				visit[v] = true;
				qu.push(v);
			}
		}
	}

	if(!visit[s])
	{
		printf("-1");
		return 0;
	}

	for (int i = 1; i <= n; i++) 
	{
		if(!visit[i])
		{
			flag[i] = true;
			for (int j = head2[i]; ~j; j = edge2[j].next) 
				flag[edge2[j].v] = true;
		}
	}

	if(flag[s])
	{
		printf("-1");
		return 0;
	}

	qu.push(s);
	d[s] = 0;
	while(!qu.empty())
	{
		int now = qu.front();
		qu.pop();

		if(now == t)
		{
			printf("%d", d[t]);
			return 0;
		}

		for (int i = head1[now]; ~i; i = edge1[i].next) 
		{
			int v = edge1[i].v;
			if(flag[v])continue;
			if(!d[v])
			{
				d[v] = d[now] + 1;
				qu.push(v);
			}
		}
	}

	printf("-1");
	return 0;
}
