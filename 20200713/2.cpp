#include<cstdio>//uncle-lu
#include<cmath>
#include<cstring>
#include<queue>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct point{
	int x, y;
};
struct HeapNode{
	int u;
	double d;
	friend bool operator<(const HeapNode a, const HeapNode b) { return a.d > b.d; }
};
struct node{
	int v, next;
	double val;
};

point line[110];
node edge[2010];
int head[110], cnt;
double d[110];
std::priority_queue<HeapNode> qu;

double func(point a, point b)
{
	return (double)sqrt( (a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y) );
}

void add_edge(int x, int y)
{
	edge[++cnt].v = y;
	edge[cnt].next= head[x];
	edge[cnt].val = func(line[x], line[y]);
	head[x] = cnt;
	return ;
}

void dijkstra(int s, int t)
{
	memset(d, 0x7f7f7f, sizeof(d));
	d[s] = 0;
	HeapNode now;
	now.u = s; now.d = 0;
	qu.push(now);
	while(!qu.empty())
	{
		now = qu.top();
		qu.pop();
		if(now.d != d[now.u])continue;
		for (int i = head[now.u]; ~i; i = edge[i].next) 
		{
			if( d[edge[i].v] > d[now.u] + edge[i].val)
			{
				d[edge[i].v] = d[now.u] + edge[i].val;
				qu.push((HeapNode){edge[i].v, d[edge[i].v]});
			}
		}
	}

	printf("%.2lf", d[t]);
	return ;
}

int main()
{
	memset(head, -1, sizeof(head));
	int n, m;
	read(n);
	for (int i = 1; i <= n; i++) 
	{
		read(line[i].x); read(line[i].y);
	}
	read(m);
	int a, b;
	for (int i = 1; i <= m; i++) 
	{
		read(a);read(b);
		add_edge(a, b);
		add_edge(b, a);
	}

	read(a);read(b);
	dijkstra(a, b);

	return 0;
}
