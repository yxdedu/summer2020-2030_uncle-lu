#include<cstdio>//uncle-lu
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

long long int k, m, n, a, b;
bool flag;

int main()
{
	read(k); read(m); read(n);
	for (int i = 1; i <= m; i++) 
	{
		read(a);read(b);
		if(a == 0)
		{
			flag = true;
			printf("%d ", i);
			continue;
		}
		if(k/a*b >= n)
		{
			flag = true;
			printf("%d ", i);
		}
	}
	if(!flag)
		printf("-1");
	return 0;
}
