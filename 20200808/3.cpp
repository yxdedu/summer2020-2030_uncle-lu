#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}	

const int mod = 998244353;
int n, m, k;
int F[210][510];
bool map[510][510];

int main()
{
	read(n);read(m);read(k);

	if(k == 0)
	{
		long long int last_x = 0, last_y = 0, now_x = 0, now_y = 0;
		last_x = 1;
		for (int i = 1; i <= m; i++) 
		{
			now_x = ( last_y * (n-1) )%mod;
			now_y = ( last_x + (last_y * (n-2)) )%mod;
			last_x = now_x;
			last_y = now_y;
		}
		printf("%lld", now_x);
		return 0;
	}

	int x, y;
	for (int i = 1; i <= k; i++) 
	{
		read(x);read(y);
		map[x][y] = true;
	}
	for (int i = 1; i <= n; i++) 
		map[i][i] = true;

	F[0][1] = 1;
	for (int i = 1; i <= m; i++) 
	{
		for (int j = 1; j <= n; j++) 
		{
			for (int a = 1; a <= n; a++) 
			{
				if(!map[a][j])
				{
					F[i][j] += F[i-1][a];
					F[i][j] %= mod;
				}
			}
		}
	}

	printf("%d", F[m][1]);

	return 0;
}
