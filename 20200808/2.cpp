#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n, m, ans;
int F[110][110];

int main()
{
	read(n);read(m);
	int temp;
	for (int i = 1; i <= n; i++) 
	{
		for (int j = 1; j <= m; j++) 
		{
			read(temp);
			if(temp)
			{
				F[i][j] = std::min(F[i-1][j], std::min(F[i][j-1], F[i-1][j-1])) + 1;
				ans = std::max(ans, F[i][j]);
			}
		}
	}

	printf("%d", ans);
	return 0;
}
