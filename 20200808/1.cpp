#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n, m;
int line[200010];
int F[200010];

int main()
{
	read(n);read(m);
	int temp;
	for (int i = 1; i <= m; i++) 
		read(line[i]);
	for (int i = 2; i <= n; i++) 
		for (int j = 1; j <= m; j++) 
		{
			read(temp);
			line[j] = std::max(line[j], temp);
		}

	int ans = line[1];
	F[1] = line[1];
	for (int i = 2; i <= m; i++) 
	{
		F[i] = std::max(line[i], F[i-1] + line[i]);
		ans = std::max(ans, F[i]);
	}

	printf("%d", ans);

	return 0;
}
