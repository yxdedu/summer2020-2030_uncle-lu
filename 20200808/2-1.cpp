#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n, m;
int sum[110][110];

int main()
{
	read(n);read(m);
	int temp;
	for (int i = 1; i <= n; i++) 
		for (int j = 1; j <= m; j++) 
		{
			read(temp);
			sum[i][j] = sum[i-1][j] + sum[i][j-1] - sum[i-1][j-1] + temp;
		}

	int ans = 0;
	for (int i = 1; i <= n; i++) 
	{
		for (int j = 1; j <= m; j++) 
		{
			for (int len = 1; len <= std::min(i, j); len++) 
			{
				int x = i - len, y = j - len;
				if(sum[i][j] - sum[i][y] - sum[x][j] + sum[x][y] == len*len)
					ans = std::max(ans, len);
			}
		}
	}

	printf("%d", ans);
	return 0;

}
