#include<cstdio>//uncle-lu
#include<algorithm>

void DFS(int);
bool check(int, int);

struct node{
	int son[2], val;
};
node Tree[1000010];
int n, ans;
int count[1000010];

bool check(int x, int y)
{
	if(x==-1 && y==-1)
		return true;

	if(x==-1 || y==-1 || Tree[x].val != Tree[y].val)
		return false;

	if( (!check(Tree[x].son[0], Tree[y].son[1])) || (!check(Tree[x].son[1], Tree[y].son[0])) )
		return false;

	return true;
}

void DFS(int x)
{
	if(Tree[x].son[0]==-1 && Tree[x].son[1]==-1)
	{
		count[x] = 1;
		ans = std::max(ans, 1);
		return ;
	}

	if(Tree[x].son[0] != -1)
	{
		DFS(Tree[x].son[0]);
		count[x] += count[Tree[x].son[0]];
	}
	if(Tree[x].son[1] != -1)
	{
		DFS(Tree[x].son[1]);
		count[x] += count[Tree[x].son[1]];
	}
	count[x] ++;

	if(check(Tree[x].son[0], Tree[x].son[1]))
	{
		ans = std::max(ans, count[x]);
	}
	
	return ;
}

int main()
{
	scanf("%d",&n);
	for(int i=1;i<=n;++i)
		scanf("%d",&Tree[i].val);

	for(int i=1;i<=n;++i)
		scanf("%d%d",&Tree[i].son[0],&Tree[i].son[1]);

	DFS(1);

	printf("%d",ans);

	return 0;
}
