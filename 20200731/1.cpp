#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}	

int line[100010];
int n, sum;

int main()
{
	read(n);
	for (int i = 1; i <= n; i++) 
		read(line[i]);

	for (int i = 1; i <= n; i++) 
		if(line[i] > line[i-1])
			sum += (line[i] - line[i-1]);

	printf("%d", sum);

	return 0;
}
