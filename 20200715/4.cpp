#include<cstdio>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int m[510][510];
int n;

int main()
{
	read(n);
	for (int i = 1; i <= n; i++) 
		for (int j = i+1; j <= n; j++) 
		{
			read(m[i][j]);
			m[j][i] = m[i][j];
		}

	int ans = 0;
	for (int i = 1; i <= n; i++) 
	{
		std::sort(m[i]+1, m[i]+1+n);
		ans = std::max(ans, m[i][n-1]);
	}

	printf("1\n%d",ans);

	return 0;
}
