#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n,m,x;
int a[360];
int F[50][50][50][50];
int c1,c2,c3,c4;

int main()
{
	read(n);read(m);
	for(int i=1;i<=n;++i)
		read(a[i]);
	for(int i=1;i<=m;++i)
	{
		read(x);
		if(x==1)c1++;
		if(x==2)c2++;
		if(x==3)c3++;
		if(x==4)c4++;
	}
	F[0][0][0][0] = a[1];
	for(int i1=0;i1<=c1;++i1)
		for(int i2=0;i2<=c2;++i2)
			for(int i3=0;i3<=c3;++i3)
				for(int i4=0;i4<=c4;++i4)
				{
					int l=1*i1+2*i2+3*i3+4*i4+1;
					if(i1)F[i1][i2][i3][i4] = std::max(F[i1-1][i2][i3][i4]+a[l],F[i1][i2][i3][i4]);
					if(i2)F[i1][i2][i3][i4] = std::max(F[i1][i2-1][i3][i4]+a[l],F[i1][i2][i3][i4]);
					if(i3)F[i1][i2][i3][i4] = std::max(F[i1][i2][i3-1][i4]+a[l],F[i1][i2][i3][i4]);
					if(i4)F[i1][i2][i3][i4] = std::max(F[i1][i2][i3][i4-1]+a[l],F[i1][i2][i3][i4]);
				}

	printf("%d",F[c1][c2][c3][c4]);
	return 0;
}