#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct node{// 导弹信息
	long long int dis1, dis2;
	friend bool operator<(const node a,const node b)
	{
		return a.dis1 > b.dis1;
	}
};
node line[100010];
int n;
int x1,y1,x2,y2;

int main()
{
	read(x1);read(y1);read(x2);read(y2);
	read(n);
	int x, y;
	for (int i = 1; i <= n; i++) 
	{
		read(x);read(y);
		line[i].dis1 = (x1 - x) * (x1 - x) + (y1 - y) * (y1 - y);
		line[i].dis2 = (x2 - x) * (x2 - x) + (y2 - y) * (y2 - y);
	}

	std::sort(line+1, line+1+n);
	long long int mx = 0, ans = 0x7f7f7f7f;
	for (int i = 1; i <= n; i++) 
	{
		mx = std::max(line[i-1].dis2, mx);
		ans = std::min(ans, line[i].dis1 + mx);
	}

	printf("%lld", ans);
	return 0;
}
