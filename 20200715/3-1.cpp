#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct node{
	int d1, d2;
};
node line[100010];
int x1, y1, x2, y2;

int main()
{
	read(x1);read(y1);read(x2);read(y2);
	int n, x, y;
	read(n);
	for (int i = 1; i <= n; i++) 
	{
		read(x);read(y);
		line[i].d1 = (x - x1) * (x - x1) + (y - y1) * (y - y1);
		line[i].d2 = (x - x2) * (x - x2) + (y - y2) * (y - y2);
	}
	int ans = 0x7f7f7f7f;
	for (int i = 1; i <= n; i++) //枚举第一个发射站的工作距离
	{
		int temp1 = line[i].d1;
		for (int j = 1; j <= n; j++) //枚举第二个发射站的工作距离
		{
			int temp2 = line[j].d2;
			bool flag = true; //true 这个工作范围是合法的
			for (int c = 1; c <= n; c++) //判断是否所有的导弹都被在工作范围内
			{
				if(temp1 < line[c].d1 && temp2 < line[c].d2) 
				{
					flag = false;//不是在工作范围内
					break;
				}
			}
			if(flag)//工作范围合法
				ans = std::min(ans, temp1 + temp2);
		}
	}
	printf("%d", ans);
	return 0;
}
