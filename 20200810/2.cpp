#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n, m;
int F[110][110];
int line[110];

int main()
{
	read(n);read(m);
	for (int i = 1; i <= m; i++) 
		read(line[i]);
	std::sort(line+1, line+1+m);
	line[m+1] = n+1;
	for (int len = 1; len <= m; len++) 
	{
		for (int l = 1; l <= m-len+1; l++) 
		{
			int r = l + len - 1;
			F[l][r] = 0x7f7f7f7f;
			for (int k = l; k <= r; k++) 
				F[l][r] = std::min(F[l][r], F[l][k-1] + F[k+1][r] + line[r+1] - line[l-1] - 2);
		}
	}

	printf("%d", F[1][m]);
	return 0;
}
