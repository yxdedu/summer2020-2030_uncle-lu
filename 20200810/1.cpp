#include<cstdio>//uncle-lu
#include<cstring>
#include<algorithm>

int start[5][5], now[5][5];

void change(int x, int y)
{
	now[x][y] = !now[x][y];
	now[x+1][y] = !now[x+1][y];
	now[x-1][y] = !now[x-1][y];
	now[x][y-1] = !now[x][y-1];
	now[x][y+1] = !now[x][y+1];
	return ;
}

bool check()
{
	for (int i = 1; i <= 3; i++) 
		for (int j = 1; j <= 3; j++) 
			if(!now[i][j])return false;

	return true;
}

int main()
{
	for (int i = 1; i <= 3; i++) 
		for (int j = 1; j <= 3; j++) 
			scanf("%d", &start[i][j]);

	int x, y, ans = 10;
	for (int i = 0; i <= 1023; i++) //枚举二进制位的情况.
	{
		int temp = i, cnt = 0;
		memcpy(now, start, sizeof(start));// start ---> now
		for (int j = 0; j <= 8; j++) 
		{
			if(temp & 1)
			{
				x = j/3 + 1, y = j%3 + 1;
				change(x, y);
				cnt++;
			}
			temp >>= 1;
		}
		if(check())
			ans = std::min(ans, cnt);
	}

	printf("%d", ans);
	return 0;
}
