#include<cstdio>//uncle-lu
#include<queue>
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

struct node{
	int s, t;
	//s 表示国籍， t表示时间
};
std::queue<node> qu;
int n, t, m, ans;
int tong[100010];

int main()
{
	read(n);
	for (int i = 1; i <= n; i++) 
	{
		read(t);read(m);
		while(!qu.empty())
		{
			node p = qu.front();
			if(p.t+86400 <= t)
			{
				tong[p.s]--;
				if(tong[p.s] == 0)ans--;
				qu.pop();
				continue;
			}
			break;
		}
		int temp;
		for (int j = 1; j <= m; j++) 
		{
			read(temp);
			node p;
			p.s = temp;
			p.t = t;
			qu.push(p);
			tong[p.s]++;
			if(tong[p.s] == 1)ans++;
		}

		printf("%d\n", ans);
	}

	return 0;
}
