#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n, m;
bool visit[10][10];
int x_turn[4]={1, -1, 0, 0};
int y_turn[4]={0, 0, 1, -1};
int ans;

void DFS(int x, int y)
{
	if(x == 1 || x == n || y == 1 || y == m)
	{
		ans++;
		return ;
	}
	for (int i =  0; i <= 3; i++) 
	{
		int xx = x + x_turn[i], yy = y + y_turn[i];
		if(xx < 1 || xx > n || yy < 1 || yy > m || visit[xx][yy])continue;
		visit[xx][yy] = true;
		DFS(xx, yy);
		visit[xx][yy] = false;
	}

	return ;
}

int main()
{
	read(n);read(m);
	n++;
	m++;
	for (int i = 2; i < n; i++) 
	{
		visit[i][1] = true;
		visit[i][2] = true;
		DFS(i, 2);
		visit[i][2] = false;
		visit[i][1] = false;
	}

	for (int i = 2; i < m; i++) 
	{
		visit[1][i] = true;
		visit[2][i] = true;
		DFS(2, i);
		visit[2][i] = false;
		visit[1][i] = false;
	}

	printf("%d", ans);
	return 0;
}
