#include<cstdio>//uncle-lu
#include<algorithm>
template<class T>void read(T &x)
{
	x=0;int f=0;char ch=getchar();
	while(ch<'0'||ch>'9') { f|=(ch=='-'); ch=getchar(); }
	while(ch<='9'&&ch>='0') { x=(x<<1)+(x<<3)+(ch^48); ch=getchar(); }
	x = f ? -x : x;
	return ;
}

int n;
int Father[110], deep[110], width[110];

int main()
{
	int x, y;
	read(n);
	for (int i = 1; i < n; i++)
	{
		read(x);read(y); 
		Father[y] = x;
		deep[y] = deep[x] + 1;
		width[ deep[y] ] ++;
	}
	read(x);read(y);

	int mx = 0;
	for (int i = 1; i <= n; i++) 
		mx = std::max(deep[i]+1, mx);
	printf("%d\n", mx);

	mx = 0;
	for (int i = 1; i <= 100; i++) 
		mx = std::max(mx, width[i]);
	printf("%d\n", mx);

	int sum = 0;
	while(deep[x] > deep[y])
	{
		x = Father[x];
		sum += 2;
	}
	while(deep[x] < deep[y])
	{
		y = Father[y];
		sum += 1;
	}
	while(x != y)
	{
		x = Father[x];
		y = Father[y];
		sum += 3;
	}

	printf("%d", sum);

	return 0;
}
