#include<cstdio>//uncle-lu
#include<algorithm>

double line[10010];
int sex[10010];

double boy[10010], girl[10010];
int top_boy, top_girl;

int main()
{
	int T, n;
	scanf("%d", &T);
	for (int t = 1; t <= T; t++) 
	{
		scanf("%d", &n);
		top_boy = 0; top_girl = 0;
		for (int i = 1; i <= n; i++) 
			scanf("%d", &sex[i]);
		for (int i = 1; i <= n; i++) 
			scanf("%lf", &line[i]);

		for (int i = 1; i <= n; i++)
			if(sex[i])
				girl[++top_girl] = line[i];
			else
				boy[++top_boy] = line[i];

		std::sort(girl+1, girl+1+top_girl);
		std::sort(boy+1, boy+1+top_boy);

		for (int i = 1; i <= top_boy; i++) 
			printf("%g ", boy[i]);
		printf("\n");
		for (int i = 1; i <= top_girl; i++) 
			printf("%g ", girl[i]);
		printf("\n");
	}

	return 0;
}
